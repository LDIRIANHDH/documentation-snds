# ATU - Autorisation temporaire d'utilisation
<!-- SPDX-License-Identifier: MPL-2.0 -->

Des spécialités pharmaceutiques qui ne bénéficient pas d’une <link-previewer href="AMM.html" text="autorisation de mise sur le marché" preview-title="AMM - Autorisation de mise sur le marché" preview-text="Pour être commercialisée, une spécialité pharmaceutique doit obtenir préalablement une autorisation de mise sur le marché (AMM). " /> peuvent, à titre exceptionnel, faire l’objet d’autorisations temporaires d’utilisation (ATU) délivrées par l’<link-previewer href="ANSM.html" text="ANSM" preview-title="ANSM - Agence nationale de sécurité du médicament" preview-text="L'ANSM est l'Agence nationale de sécurité du médicament et des produits de santé." /> si elles sont destinées à traiter des maladies graves ou rares, en l’absence de traitement approprié, lorsque la mise en œuvre du traitement ne peut être différée.

::: warning Attention
Ne pas confondre avec le forfait d'accueil et de traitement des urgences, aussi appelé ATU (variable `PRS_NAT_REF` = 2238 dans la table `ER_PRS_F`).
:::

## Références

- [Page sur le site du ministère de la santé](https://solidarites-sante.gouv.fr/soins-et-maladies/medicaments/professionnels-de-sante/autorisation-de-mise-sur-le-marche/article/autorisations-temporaires-d-utilisation-atu)
- [Page sur le site de l'ANSM](https://www.ansm.sante.fr/Activites/Autorisations-temporaires-d-utilisation-ATU/Qu-est-ce-qu-une-autorisation-temporaire-d-utilisation/(offset)/0)
- [Page wikipedia](https://fr.wikipedia.org/wiki/Autorisation_temporaire_d%27utilisation)
