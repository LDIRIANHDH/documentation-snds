# Codage
<!-- SPDX-License-Identifier: MPL-2.0 -->

Le codage de l'information médicale est l'activité consistant à transformer des données médicales textuelles en des codes tirés de classifications, telles que la <link-previewer href="CCAM.html" text="CCAM" preview-title="CCAM - Classification commune des actes médicaux" preview-text="La classification commune des actes médicaux est une nomenclature française destinée à coder les gestes pratiqués par les médecins, gestes techniques dans un premier temps puis, par la suite, les actes intellectuels cliniques. " /> ou la <link-previewer href="CIM.html" text="CIM" preview-title="CIM - Classification internationale des maladies" preview-text="La classification internationale des maladies, publiée par l’Organisation mondiale de la santé (OMS est utilisée pour coder les diagnostics dans les recueils d’information des différents domaines du PMSI. " />. 

## Références

- Site [AideAuCodage.fr](https://www.aideaucodage.fr)
