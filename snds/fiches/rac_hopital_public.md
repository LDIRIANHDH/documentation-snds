# Reste à charge après AMO en établissements de santé publics
<!-- SPDX-License-Identifier: MPL-2.0 -->

Cette fiche explique comment calculer le montant du reste à charge après Assurance maladie obligatoire (AMO) pour un séjour en établissement de santé public à partir du PMSI.

Les explications sont déclinées par spécialité hospitalière :

* [MCO](../glossaire/MCO.md) : médecine chirurgie obstétrique et odontologie
* [SSR](../glossaire/MCO.md) : soins de suite et de réadaptation
* [HAD](../glossaire/HAD.md) : hospitalisation à domicile

Nous ne traitons pas ici le calcul du reste à charge en psychiatrie.  

::: warning ATTENTION
Nous partageons l’information que nous sommes parvenus à recueillir sur le calcul du reste à charge.  
Celle-ci n’est pas exhaustive et doit être traitée avec précaution.  
N'hésitez pas à suggérer des modifications ou à poser vos questions sur la communauté d'entraide du SNDS.
:::


**Définitions :**  

Le schéma ci-dessous résume les notions importantes pour une dépense présentée au remboursement :  
[](../files/DREES/2020_01_schema_rac_amo_hospit.png)
<img src="../files/DREES/2020_01_schema_rac_amo_hospit.png"  width="430" height="300">

*Signification des abréviations présentes sur le schéma :*  
**AMO** : assurance maladie obligatoire; **BRSS** : base de remboursement de la sécurité sociale;   
**FJ** : forfait hospitalier journalier; **PF** : participation forfaitaire (pour actes lourds / coûteux);  
**RAC** : reste-à-charge; **TM** : ticket modérateur   



L'ensemble des dépenses associées à un séjour en établissement public comprend :

* le **montant AMO** qui est la part légale prise en charge par l'assurance maladie obligatoire (AMO)  
  (*cf.* fiche sur [les dépenses des établissements de santé publics dans le PMSI](../fiches/depenses_hopital_public.md) 
  pour plus d'informations sur le montant AMO).
* le **reste à charge après AMO** payé par le patient et / ou son organisme complémentaire, lui même composé :
  * du **RAC opposable**, qui est la différence entre le tarif de convention (Montant BRSS) et le remboursement de la Sécurité sociale (montant AMO)
  * d'éventuels **dépassements** d'honoraires

Le montat AMO ici défini exclut les parts supplémentaires prises en charge par le public (Sécurité sociale, Etat, ou organismes assurant les remboursements au titre de la CMU-C)
et qui couvrent l'intégralité du RAC opposable à l'hôpital pour les bénéficiaires de la CMU-C, de l'AME, des soins urgents, 
ainsi que pour les détenus.  


## Composantes et modalités de calcul du reste à charge 

À l'hôpital, le reste à charge après AMO (RAC AMO) correspond à la participation due par le patient à l'hébergement et aux frais de soins : 
- La participation à l’hébergement consiste en un **forfait journalier hospitalier**.  
- La participation aux prestations de soins peut prendre la forme : soit d’un **ticket modérateur** (TM), soit d’une **participation forfaitaire**.  
  D'éventuels **dépassements d'honoraires** peuvent s'y ajouter dans le cadre de l'activité libérale des praticiens hospitaliers. 

Le patient peut s'acquitter en outre de frais liés aux suppléments pour confort personnel (*e.g.* mise à disposition d'une chambre particulière). Ces frais ne sont pas pris en charge par l'assurance maladie, et ne figurent donc pas de manière  exhaustive dans le SNDS. 

Nous allons d'abord présenter les différentes composantes du RAC AMO, puis expliquer leurs règles d'imputation, afin de calculer le RAC AMO dans les différents cas de figure.  


### Les différents composants du reste à charge

#### Le forfait journalier hospitalier

Le **forfait journalier hospitalier** représente la participation financière du patient aux frais d'hébergement et d'entretien entraînés par son hospitalisation. 

Il est dû pour chaque journée d'hospitalisation, pour tout séjour supérieur à 24 heures dans un établissement hospitalier public ou privé, y compris le jour de sortie, sauf en cas de décès ou transfert.  
Il est également dû en cas de séjour inférieur à 24 heures, si celui-ci est à cheval sur 2 journées calendaires.  
Dans ce cas-là, on compte le jour d'entrée et le jour de sortie, soit deux jours (règle de "présence à minuit" même si la présence réelle est inférieure à 24 heures).

Le montant du forfait hospitalier est fixé par arrêté ministériel.

Depuis le [1er janvier 2018](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000036251896&categorieLien=id), il est de :
- 20 €  par jour en hôpital ou en clinique (18 € avant 2018)
- 15 € par jour dans le service psychiatrique d'un établissement de santé (13,5 € avant 2018)

Le forfait journalier hospitalier **n'est pas remboursé par l'assurance maladie**. 
Il peut éventuellement être pris en charge par les organismes complémentaires en santé.

#### Le ticket modérateur

Pour un assuré de droit commun hospitalisé dans le public, l'assurance maladie rembourse 80 % du montant des prestations de soins : c'est le **montant AMO** ; et 
le patient (ou son organisme complémentaire) participe à hauteur de 20 % du montant des prestations de soins : c'est le **ticket modérateur**.  
Le taux de remboursement peut varier entre 80 et 100 % en fonction du profil du patient, du type d'acte et de la nature de l'assurance.  


Attention, dans le secteur public, il existe deux manières distinctes de calculer le montant des prestations de soins :
- Pour le calcul du **montant AMO**, le montant des prestations de soins est calculé à partir des **tarifs nationaux de prestations** attribués à chaque séjour 
  depuis la mise en place de la T2A : il s'agit des groupes homogènes de séjour ([GHS](../glossaire/GHS.md)) en MCO, des groupes homogènes de tarifs ([GHT](../glossaire/GHT.md)) en HAD et 
  des groupes médico-économiques ([GMT](../glossaire/GMT.md)) en SSR.    

- Pour le calcul du **ticket modérateur**, le montant des prestations de soins est calculé à partir du tarif journalier de prestation propre à chaque catégorie tarifaire (TJP), multiplié par la durée de séjour (cette durée ne pouvant dépasser 30 jours). 
Le TJP est défini à l’échelle d’un établissement par arrêté de l’agence régionale de santé (ARS) et doit être représentatif du coût réel du séjour. 
Il repose sur une estimation des charges d'exploitations et de l'activité prévisionnelles de l'établissement, en fonction des soins effectués. Les charges prises en compte sont : les dépenses de personnel, les dépenses médicales, les dépenses hôtelières, les frais de gestion et autres charges d’exploitation qui ne sont pas couvertes par des ressources propres. 
Le montant des prestations de soins calculé par l'établissement est généralement plus élevé que celui de l'assurance maladie.

*Point réglementaire :*  
Lors de la mise en œuvre de la T2A dans les établissements de santé, un dispositif transitoire a été mis en place permettant aux hôpitaux publics et aux établissements privés à but non lucratif de continuer à calculer le ticket modérateur sur la base des tarifs journaliers de prestations (TJP), initialement définis à partir du coût de revient prévisionnel des différentes catégories de soins de chaque établissement et non sur les tarifs nationaux de prestations (GHS, GHT, GMT).   
L'[article 26 du PLFSS de 2020](http://www.assemblee-nationale.fr/15/projets/pl2296.asp) vise à mettre fin à cette situation transitoire :  
- en pérennisant le système du calcul du ticket modérateur sur la base des tarifs journaliers de prestations pour les établissements concernés
- en rationalisant ce système de calcul par l'introduction d'une nomenclature simplifiée et nationale des tarifs de prestations, afin de s’assurer que ceux-ci reflètent davantage le coût effectif du service

Cette réforme entrera progressivement en vigueur à partir de 2021.


#### La participation forfaitaire des assurés pour les actes lourds 

Un ticket modérateur forfaitaire sur les actes lourds a été instauré par la [Loi de Financement de la Sécurité Sociale pour 2006](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000815414) 
et le [Décret N° 2006-707 du 19 juin 2006](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000000792524) paru au JO du 20 juin 2006. 
Cette mesure prévoit que les patients, jusqu’ici exonérés en raison de la valeur des actes réalisés, acquittent une participation minimale sur les actes médicaux dont le montant est supérieur ou égal à 120 euros ou sur les actes affectés d’un coefficient supérieur ou égal à 60. Les cas particuliers concernant la facturation du ticket modérateur forfaitaire sont détaillés [ici](https://www.ameli.fr/assure/remboursements/reste-charge/forfait-24-euros). À noter qu'en cas d'hospitalisation (à l'hôpital ou en clinique), lorsqu'un ou plusieurs actes d'un tarif supérieur ou égal à 120 euros, ou ayant un coefficient supérieur ou égal à 60, sont effectués pendant le séjour, la participation forfaitaire ne s'applique qu'une seule fois par séjour, pour l'ensemble des frais d'hospitalisation.  

La valeur de la participation forfaitaire dépend de l'année dans laquelle on se situe, elle est fixée par décret.  
Elle est passée de 18 à 24 euros au [1er janvier 2019](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037864770&categorieLien=id).

*En pratique* : dès lors qu'il y a un acte coûteux, il y a exonération du ticket modérateur qui est remplacé par la participation forfaitaire et l'établissement reçoit 100 % du tarif national de prestation de la part de l'assurance maladie.    
Le patient s'acquitte de la participation forfaitaire et du forfait journalier (sauf cas particuliers avec exonération).  

#### L'activité libérale des praticiens hospitaliers

Les praticiens statutaires exerçant à temps plein sont autorisés à exercer une activité 
libérale dans l'établissement public de santé où ils sont nommés sous certaines conditions (*cf.* [fiche sur l’activité libérale des praticiens statutaires](https://solidarites-sante.gouv.fr/IMG/pdf/activite_liberale_des_praticiens_statutaires_exercant_a_temps_plein-3.pdf)).  
Un patient peut choisir d’être soigné ou hospitalisé dans le cadre de l’activité d’un praticien statutaire exerçant à temps plein. 
Ce dernier doit l’informer des tarifs et des éventuels dépassements. 
L’activité libérale peut concerner les consultations externes, les actes médico-techniques et les soins en hospitalisation.  
Les praticiens exerçant une activité libérale peuvent percevoir leurs honoraires soit directement, soit par l’intermédiaire de l’administration de l’hôpital. 
Le praticien doit établir une feuille de soins et y porter la cotation des actes réalisés et l’intégralité des honoraires demandés. 
Ainsi, cette information remonte bien à l'assurance maladie. 





### La participation du patient aux frais hospitaliers 

La facturation aux patients des différentes composantes du RAC dépend de plusieurs paramètres, 
notamment du type d’actes et de séjours, de la durée de séjour, du profil des patients. 
Le montant de la participation aux soins dépend également de la catégorie juridique 
de l’établissement de santé et de son mode de financement.

#### Exonération du forfait hospitalier journalier

Il existe des [motifs d'exonération](https://www.ameli.fr/assure/remboursements/reste-charge/forfait-hospitalier) 
pour le forfait journalier, ainsi que des [cas où il n'est pas applicable.](https://solidarites-sante.gouv.fr/IMG/pdf/Forfait_journalier_hospitalier.pdf)


#### Exonération du ticket modérateur et de la participation forfaitaire

[Certains actes et certains frais](https://www.ameli.fr/assure/remboursements/reste-charge/forfait-24-euros) 
sont pris en charge à 100 %.  
De plus, [certaines personnes](https://www.ameli.fr/assure/remboursements/reste-charge/forfait-24-euros) 
bénéficient d'une prise en charge à 100 % en raison de leur situation ou de leur état de santé.  
En cas de prise en charge à 100 %, il y a exonération du ticket modérateur et de la participation forfaitaire. 


#### Imputabilité du forfait journalier et du ticket modérateur (ou de la participation forfaitaire)

**Le forfait journalier hospitalier ne se cumule pas avec le ticket modérateur.**  
Le reste à charge d’un assuré de droit commun est égal **au montant le plus élevé** entre le ticket modérateur et le forfait journalier hospitalier.   
Néanmoins, le forfait dû pour le jour de sortie est toujours facturé en complément sauf en cas de décès ou de transfert. 

Pour le séjour d'un assuré de droit commun de plus de 30 jours, le ticket modérateur n'est facturé que pour les 30 premiers jours.  
Au-delà, le patient ne paie que le forfait journalier.  
Cette durée de 30 jours se cumule entre séjours contigus, y compris entre différentes spécialités hospitalières.   

*Exemples :*  
Nous prenons plusieurs cas de figure pour un bénéficiaire qui n'est pas exonéré de ticket modérateur ni de forfait journalier.   
- S'il reste 10 jours en MCO puis rentre chez lui, il paiera son ticket modérateur (20 % de la base de remboursement de l'établissement) + le forfait journalier du jour de sortie.   
  Si malheureusement il décède, il ne paiera que le ticket modérateur (et pas le forfait journalier du jour de sortie).  
- S'il reste 10 jours en MCO puis 25 jours en SSR avant de rentrer chez lui, il sera exonéra de ticket modérateur 30 jours après son admission à l'hôpital.  
Ainsi, il paiera le ticket modérateur pour le séjour en MCO et pour les 20 1er jours en SSR.  
À partir du 21ème jour d'hospitalisation en SSR, et jusqu'à la fin de son séjour, 
il paiera le forfait journalier hospitalier, y compris pour le jour de sortie.

**Le forfait journalier et la participation forfaitaire se cumulent.** 
Un patient recevant des actes lourds s'acquittera donc du montant de la participation forfaitaire et du montant du forfait journalier pour la durée totale du séjour (sauf en cas d'exonération du forfait journalier) et y compris pour le jour de sortie (sauf en cas de décès ou de transfert).


#### Règles de calcul du reste-à-charge après AMO 

Les principales règles de calcul du RAC AMO sont résumées dans le tableau ci-dessous.  

![](../files/DREES/2020-01_tableau_RAC.png)  

*Signification des sigles utilisés dans le tableau :*  
**FJU** : forfait journalier unitaire (par journée d’hospitalisation)  
**nb_jour** : nombre de jours de présence  
**PF** : participation forfaitaire  
**TNP** : tarif national de prestation (*i.e.* GHS en MCO, GMT en SSR et GHT en HAD)  
**TR** : taux de remboursement (%)  
  

( * ) facturation du FJU pour le jour de sortie si le mode de sortie est autre que transfert ou décès, 0 sinon  
( ** ) « durée + 1 » si le mode de sortie est autre que transfert ou décès (car facturation du jour de sortie), « durée » sinon  
( *** ) en cas d’hospitalisation à temps partiel ou de séjour en ambulatoire, on ne parle plus de durée mais de nombre de jours de présence  

Dans ce tableau, ne figurent pas les éventuels dépassements d'honoraires qui peuvent 
avoir lieu au cours de séjours hospitaliers dans le cadre de l'activité libérale des praticiens hospitaliers.   

Le tableau ci-dessus couvre le cas général, à partir de l'exemple du MCO, mais n'est pas exhaustif.  
Nous reviendrons sur les particularités par PMSI dans le reste de la fiche.
En voici les principales différences :  
- Il n'y a pas de forfait journalier en HAD. On est donc systématiquement dans le cas de figure avec exonération de FJ.
- En MCO, on appelle "hospitalisation partielle" un séjour de moins de 24 heures sans nuitée. Si un même patient revient plusieurs fois à la journée, il y a autant de séjours que de journées d'hospitalisation.  
- En SSR, on appelle hospitalisation partielle un séjour sans nuitée, sans limite de durée totale. 
  Il faut donc distinguer les séjours en hospitalisation complète et partielle. 
  À noter qu'il n'y a qu'un type d'hospitalisation (complète / partielle) par séjour. Tout changement de type d'hospitalisation donne lieu à un nouveau séjour. 
- Lors de séjours en hospitalisation partielle en SSR, il n'y a pas de FJ facturé. Le temps passé à l'hôpital est indiqué par le nombre de jours de présence.    
- Lors de séjours en hospitalisation complète en SSR, le patient peut bénéficier de permissions. Ainsi, pour connaître le temps passé à l'hôpital, il ne faut pas compter la durée de séjour (*i.e.* le nombre de jours entre le début et la fin du séjour), mais le nombre de jours de présence.    

## En pratique : calcul des restes à charge hospitaliers à partir du PMSI MCO

Dans cette partie, et les suivantes, nous appliquons les règles décrites en première partie 
et proposons une méthodologie pour calculer les restes à charge (RAC) lors de séjours hospitaliers 
en établissements publics à partir du PMSI.  
Cette méthodologie est déclinée par discipline hospitalière.  

Le reste à charge lors des [ACE](../fiches/actes_consult_externes.md) 
a été traité dans la fiche sur les [dépenses de santé en établissements de santé publics](../fiches/depenses_hopital_public.md).

**Limites :**  
Le calcul du RAC présenté ci-dessous ne tient pas compte des éventuels dépassements 
d'honoraires qui peuvent avoir lieu lors des séjours. Dans ces cas-là, les honoraires 
du praticien hospitalier sont directement facturés au patient (en plus des frais associés au séjour) 
et n'apparaissent pas dans le PMSI mais dans le DCIR(S).  


Nous décrivons ci-dessous les modalités de calcul du reste à charge opposable à partir des données du PMSI MCO de 2016.  

### Les tables et variables mobilisées

Pour plus d'informations sur les tables et variables utilisées, se référer à la [fiche sur les dépenses](../fiches/depenses_hopital_public.md).

La clef de chaînage entre les tables mentionnées ci-dessous est le couple (`ETA_NUM`, `RSA_NUM`) où `ETA_NUM` est le numéro FINESS de l'établissement et `RSA_NUM` est le numéro séquentiel du séjour. 

Dans la **table de chaînage patients** `T_MCOaaC`, nous considérons les variables :
- `NIR_ANO_17` : identifiant du bénéficiaire ([fiche identifiant des bénéficiaires](../fiches/fiche_beneficiaire.md))
- `EXE_SOI_DTD` et `EXE_SOI_DTF` qui indiquent les dates d'entrée et de sortie à l'hôpital respectivement  

Dans la table `T_MCOaaVALO`, qui est la **table de valorisation des séjours** (données retraitées par l'ATIH), nous considérons les variables :  
- `MNT_18` : montant de la participation forfaitaire de 18 euros (en 2016) pour les actes exonérants.  
À noter qu’il s’agit d’une variable renseignée par l’établissement (elle n’est pas recalculée).
- `MNT_FJ2` : montant du forfait journalier (FJ).  
La variable a été corrigée par l’ATIH qui a notamment forcé à zéro le FJ dans le cas où il n’est pas applicable 
(*cf.* [manuel d'utilisation du logiciel VisualValoSej MCO](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwiRm-798-TlAhUPFRQKHQHkBi8QFjACegQIABAH&url=https%3A%2F%2Fwww.atih.sante.fr%2Fplateformes-de-transmission-et-logiciels%2Flogiciels-espace-de-telechargement%2Ftelecharger%2Fgratuit%2F8758%2F1745&usg=AOvVaw3nx5ugXpZiyo3SBrv_M4is))
- `MNT_GHS_AM` : valorisation AM du [GHS](../glossaire/GHS.md)
- `MNT_TOT_AM` : coût pour l’assurance maladie obligatoire (GHS + suppléments journaliers en sus du GHS)  
  Ce montant ne tient cependant pas compte du paiement des médicaments et dispositifs médicaux de la liste en sus. Pour retrouver ces montants, se référer aux fiches sur [les médicaments](../fiches/depenses_hopital_public.md) et [les dispositifs](../fiches/dispositifs_medicaux_implantables_en_sus.md) de la liste en sus.  

- `TAUX2` : taux de remboursement du séjour 
- `VALO` : valorisation du séjour, prenant les valeurs suivantes :
  * 0 : si le séjour n’est pas valorisé 
  * 1 : si le séjour est valorisé 
  * 2 : dans le cas d’un séjour non valorisé avec prélèvement d’organes. Dans ce cas, seuls les prélèvements d’organes sont valorisés pour le séjour. 
  * 3 : si le séjour est en AME (Aide Médicale d'Etat)
  * 4 : si le séjour est en Soins urgents (SU)
  * 5 : si le patient est un détenu

Dans la table `T_MCOaaSTC`, qui est la **table de prise en charge des séjours dans le public** (données brutes renseignées par l'établissement) :
- `REM_BAS_MNT` : coût du séjour calculé par l'établissement (sert de base de remboursement pour le ticket modérateur) 
- `TOT_MNT_AM` : coût du séjour facturé à l'AM selon les règles de calcul de l'établissement (différent du montant remboursé par l'AM qui sera calculé sur la base du GHS, *cf.* [fiche sur les dépenses de santé](../fiches/depenses_hopital_public.md)) 
- `FAC_MNT_TM` : montant à facturer au titre du ticket modérateur (TM)
- `FAC_MNT_FJ` : montant à facturer au titre du forfait journalier (FJ)
- `NAT_ASS` : nature d’assurance, qui prend les valeurs suivantes (d'après la norme B2) : 
  - 10 : maladie
  - 13 : maladie régime local (Alsace – Moselle)
  - 30 : maternité
  - 41 : accident du travail
  - 90 : prévention maladie
  - XX : non applicable ou sans objet  
- `EXO_TM` : code justification d’exonération du ticket modérateur qui prend les valeurs suivantes (d'après l'annexe 9 du cahier des charges de la norme B2):
  - 0 : pas d’exonération
  - 1 : soins en rapport avec un K ou un KC >= 50 (NGAP) (à compter du 01/01/2004, les actes en rapport avec un acte "K ou Z >= 50" non présents sur la facture ne sont plus exonérés)
  - 2 : soins relatifs à un séjour >30 jours (réservé à un établissement)
  - 3 : soins particuliers exonérés (traitement de la stérilité, soins aux prématurés, actes exonérants, soins en établissements des nourrissons dans les 30 premiers jours, dépistage du VIH)
  - 4 : soins conformes au protocole ALD
  - 5 : assuré ou bénéficiaire exonéré (régime exonérant)
  - 6 : réservé SESAm-VITALE pour les régimes spéciaux SNCF
  - 7 : soins dispensés en risque maladie et exonérés dans le cadre d'un dispositif de prévention
  - 8 : réservé régime général
  - 9 : FSV ou FSI (ancien FNS)
  - C : soins exonérés en codage CCAM du fait de la nature de l'acte, du dépassement du seuil
  - X : non applicable ou manquant
- `FJ_COD_PEC` : code de prise en charge du forfait journalier, prenant les valeurs suivantes (d'après l'annexe 16 du cahier des charges de la norme B2) :
  * A : si à la charge de l’assuré
  * X : si pas d’information sur le forfait journalier
  * R : si à la charge du régime obligatoire
  * L : si à la charge du régime Alsace-Moselle
- `FAC_18E` : facturation (1) ou non (0) de la participation forfaitaire de 18 € (2016)
- `REM_TAU`: taux de remboursement du séjour


Dans la table `T_MCOaaB`, qui est la **table de description du séjour**, nous considérons la variable:
- `ENT_MOD` : mode d'entrée dans le champ PMSI MCO 
- `SOR_MOD` : mode de sortie du champ PMSI MCO qui peut prendre les valeurs suivantes :
  * -1  Valeur inconnue
  * -2  Sans objet
  * 0   Transfert pour ou après réalisation d'un acte
  * 6   Mutation (transfert au sein du même établissement vs entre établissements).
  * 7   Transfert normal
  * 8   Domicile
  * 9   Décès
- `GRG_GHM`: groupe homogène de malades attribué au séjour
- `TYP_GEN_RSA` : type de génération automatique du résumé de sortie anonyme

::: warning ATTENTION
À partir de 2019, le montant du ticket modérateur forfaitaire (TMF) passe de 18 à 20 € et les variables `MNT_18` (qui n'existe qu'en MCO) et `FAC_18E` (tous PMSI) sont remplacées par `MNT_TMF` et `TOP_TMF` respectivement.     
:::

### Filtres à ajouter

Les filtres à poser pour extraire les informations sur les dépenses et le reste à 
charge lors de séjours en MCO en établissements publics sont détaillées dans la 
[fiche sur les dépenses à l'hôpital public](../fiches/depenses_hopital_public.md).


### Méthodologie d'exploitation du PMSI MCO pour le calcul du reste à charge

#### Nettoyage des taux de remboursement

Deux variables renseignent le taux de remboursement :
- Variable `REM_TAU` (table `T_MCOaaSTC`) fournie par l'établissement
- Variable `TAUX2` (table `T_MCOAaaVALO`), qui est la version de `REM_TAU` corrigée par l'ATIH

Dans les cas où la variable `TAUX2` est manquante, il est possible de le reconstruire comme indiqué dans [la documentation de l'ATIH](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwiRm-798-TlAhUPFRQKHQHkBi8QFjACegQIABAH&url=https%3A%2F%2Fwww.atih.sante.fr%2Fplateformes-de-transmission-et-logiciels%2Flogiciels-espace-de-telechargement%2Ftelecharger%2Fgratuit%2F8758%2F1745&usg=AOvVaw3nx5ugXpZiyo3SBrv_M4is),
à partir du code d'exonération du TM (`EXO_TM`) et de la nature de l'assurance (`NAT_ASS`).  
La variable `tx_ATIH` peut être calculée comme suit :  
  - Si `EXO_TM` est dans [0,2] et `NAT_ASS` est dans [10,13] alors `tx_ATIH` prend la valeur de 80 %
  - Si `EXO_TM` est dans [0,2] et `NAT_ASS` n'est pas dans [10,13] alors `tx_ATIH` prend la valeur de 100 %
  - Si `EXO_TM` est dans [9] et `NAT_ASS` est dans [10] alors `tx_ATIH` prend la valeur de 90 %
  - Si `EXO_TM` ou `NAT_ASS` est manquant `tx_ATIH` est manquant
  - Dans les autres cas `tx_ATIH` prend la valeur de 100 %  

L'ATIH suggère aussi de supprimer les lignes pour lesquelles `tx_ATIH` ne peut être calculé du fait de valeurs manquantes pour `EXO_TM` ou `NAT_ASS`.

*Suggestion :*  
Dans le cas général, nous suggérons la création de la variable corrigée `TAUX_C` comme suit :
1. `TAUX_C` est égal à `TAUX2` (sauf si manquant ou nul hors prélèvement d’organe)
2. Si prélèvement d’organe, attribuer à `TAUX_C` la valeur de 0 % (code VALO=2)  
3. Si `TAUX2` manquant ou nul (hors prélèvement d’organe) : `TAUX_C` est égal à `tx_ATIH`
4. Si `TAUX2` et `tx_ATIH` sont manquants, supprimer la ligne

Dans le cas particulier des bénéficiaires de l'AME, de SU ou des détenus (`VALO` IN (3,4,5)),
`TAUX2`et `REM_TAU` présentent toutes deux beaucoup de valeurs nulles ou manquantes, mais 
`REM_TAU` semble tout de même mieux renseignée.  
Pour ces populations, les étapes 1. et 4. deviennent :   
1. `TAUX_C` est égal à `REM_TAU` (sauf si manquant ou nul hors prélèvement d’organe)
4. Si `TAUX2` et `tx_ATIH` sont manquants, forcer le taux de remboursement à 100 %

#### Nettoyage du forfait journalier

Deux variables renseignent le montant du forfait journalier :
- Variable `FAC_MNT_FJ` (table `T_MCOaaSTC`)
- Variable `MNT_FJ2` (table `T_MCOaaVALO`), qui est la version de `FAC_MNT_FJ` corrigée par l'ATIH

*Suggestion :* 
Il est également possible de recalculer le montant du forfait journalier à partir de la durée de séjour (variable `duree_sej` calculée à partir des dates d'entrée et de sortie), du code d'exonération du forfait journalier (`FJ_COD_PEC`), des frais d'hébergement par journée d’hospitalisation (18€ en 2016) et du mode de sortie (`SOR_MOD`), comme suit : 
- `FJ_C`= 0 en cas de séjour en ambulatoire (entrée / sortie le même jour)
- `FJ_C`= 0 en cas d'exonération du forfait journalier (*i.e.* si `FJ_COD_PEC`="R" et si les conditions d'exonération listées dans la partie intitulée "Le forfait journalier et le forfait journalier de sortie" sont remplies)
- `FJ_C`= 18 * `duree_sej`, si transfert ou décès 
- `FJ_C` = 18 * (`duree_sej` + 1), sinon

On peut ensuite créer une variable corrigée appelée `FJ_C2` qui prend la valeur du forfait journalier effectivement facturé : 
- `FJ_C2` = 0 € si séjour en ambulatoire ou si exonération du FJ
- `FJ_C2` = 18 € (= valeur du FJ du jour de sortie en 2016) si séjour sans exonération de TM ni de FJ, de moins de 30 jours, sans transfert ni décès
- `FJ_C2` = 0 € si séjour sans exonération de TM ni de FJ, de moins de 30 jours, avec transfert ou décès
- `FJ_C2` = `FJ_C` - 30*18 (= valeur du FJ au-delà du 30ème jour d'hospitalisation) si séjour sans exonération de TM ni de FJ, de plus de 30 jours, sans transfert ni décès 
- `FJ_C2` = `FJ_C` - 29*18 (= valeur du FJ au-delà du 30ème jour d'hospitalisation + FJ du jour de sortie) si séjour sans exonération de TM ni de FJ, de plus de 30 jours, avec transfert ou décès 
- `FJ_C2` = `FJ_C` sinon (=valeur du FJ pour l'ensemble du séjour)

*ATTENTION > en cas de séjours contigus, la limite des 30 jours s'applique à la durée d'hospitalisation cumulée. Pour plus de rigueur, il faudrait chaîner les séjours pour calculer la durée de séjour, non pas par PMSI, mais depuis l'entrée à l'hôpital.*

#### Nettoyage de la participation forfaitaire

Il existe deux façons de renseigner la facturation ou non de la participation forfaitaire (qui était de 18 € en 2016) :
- Variable `FAC_18E` (table `T_MCOaaSTC`), renseignée par l'établissement
- Variable `MNT_18` (table `T_MCOaaVALO`), qui est normalement égale à `FAC_18E`*18 

*Suggestion :* 
- Privilégier `FAC_18E` qui a moins de valeurs manquantes.
- Dans les cas où la participation forfaitaire s'applique, remplacer les taux de remboursement de 80 ou 90 % (0,2 % des cas) par 100 % (on considère que les taux <100 % sont des erreurs). On attribue donc la valeur de 100 % au taux de remboursement corrigé (`TAUX_C`).
- Penser à remplacer les valeurs manquantes, s'il y en a, par des 0.

#### Nettoyage du ticket modérateur

Pour faciliter le calcul du RAC, penser à remplacer d'éventuelles valeurs manquantes de `FAC_MNT_TM` par des 0.

Pour le séjour d'un assuré de droit commun de plus de 30 jours, le ticket modérateur n'est facturé que pour les 30 premiers jours.  
Notre observation des données suggère qu'en cas de séjours longs, la variable `FAC_MNT_TM` n'est pas plafonnée à 30 jours, 
mais correspond à 20 % de la base de remboursement de l'établissement (TJP) sur la durée totale du séjour.  

*Suggestion :*  
Nous suggérons la création de la variable corrigée `TM_C` comme suit :  
1. Pour le séjour d'un assuré de droit commun de plus de 30 jours :
    - Nous pensons que `FAC_MNT_TM` = 20 % * TJP * `duree_sej`
    - Nous recalculons un ticket modérateur corrigé plafonné à 30 jours :   
      `TM_C` = 20 % * TJP * 30 = `FAC_MNT_TM` * 30 / `duree_sej`
2. Dans tous les autres cas : `TM_C` = `FAC_MNT_TM`


#### Calcul du RAC

On utilise les variables suivantes :  
- `TAUX_C`: taux de remboursement du séjour (corrigé)
- `FJ_C`: montant du forfait journalier pour l'ensemble du séjour (corrigé)
- `FJ_C2`: montant du forfait journalier effectivement facturé (dont le calcul est présenté ci-dessus)
- `FAC_18E`: montant à facturer au titre de la participation forfaitaire de 18€ (table `T_MCOaaSTC`)
- `TM_C` : montant à facturer au titre du ticket modérateur (version corrigé de `FAC_MNT_TM` de la table `T_MCOaaSTC`)
- `FJ_COD_PEC`: code de prise en charge du forfait journalier (table `T_MCOaaSTC`)
- `VALO` : valorisation du séjour (table `T_MCOaaSTC`)

En MCO, on distingue 5 cas de figure pour le calcul du RAC AMO.  

::: warning CALCUL DU RAC AMO
Calcul de la variable `rac` dans les différents cas de figure :  
1. `rac` = 0 si `FJ_COD_PEC`==R & `TAUX_C`==100 (exonération de TM et de FJ)
2. `rac` = `FAC_18`*18  + `FJ_C2` si `FJ_COD_PEC`!=R & `TAUX_C`==100 (exonération de TM mais pas d'exonération de FJ)
3. `rac` = `TM_C` si `FJ_COD_PEC`==R & `TAUX_C`!=100 (exonération de FJ mais pas d'exonération de TM)
4. `rac` = `TM_C`+`FJ_C2` si `FJ_COD_PEC`!=R & `TAUX_C`!=100 & `TM_C` > `FJ_C` (aucune exonération, TM>FJ)
5. `rac` = `FJ_C` si `FJ_COD_PEC`!=R & `TAUX_C`!=100 & `TM_C` < `FJ_C` (aucune exonération, FJ>TM)
:::

Le coût total du séjour correspond au montant remboursé par l’AMO (variable `MNT_TOT_AM` de la table `T_MCOaaVALO`) auquel on ajoute le reste à charge. 

*Pour aller plus loin >*  
- Il est ensuite possible de calculer un reste à charge après AMO **par bénéficiaire** en agrégeant les RAC pour les différents séjours d'un même bénéficiaire (en utilisant le `NIR_ANO_17` de la table `T_MCOaaC`).
- Il est possible d'aller chercher les parts supplémentaires prises en charge par le public pour les 
  bénéficiaires de la CMU-C, de l'AME, des soins urgents, ainsi que pour les détenus. 
  À l'hôpital, les parts supplémentaires assurent la prise en charge du RAC opposable dans son intégralité pour ces populations.  
  Le RAC AMO (parts légale + supplémentaires) peut donc être fixé à 0.  
  Les bénéficiaires de l'AME, des soins urgents, ainsi que les détenus peuvent être identifiés à partir de la variable `VALO` dans 
  la table `T_MCOaaVALO` (3 : AME , 4 : SU, 5 : détenus).
  Les bénéficiaires de la CMU-C peuvent être identifiés comme décrit dans la fiche sur la [CMU-C](../fiches/cmu_c.md).
  

## En pratique : calcul des restes à charge hospitaliers à partir du PMSI SSR

Nous décrivons ci-dessous les modalités de calcul du reste à charge opposable à partir des données du PMSI SSR de 2016.  

### Les tables et variables mobilisées

Pour plus d'informations sur les tables et variables utilisées, se référer à la [fiche sur les dépenses](../fiches/depenses_hopital_public.md).

La clef de chaînage entre les tables mentionnées ci-dessous est le couple (`ETA_NUM`, `RHA_NUM`) où `ETA_NUM` est le numéro FINESS de l'établissement 
et `RHA_NUM` le numéro séquentiel du séjour. 

Dans la **table de chaînage patients** `T_SSRaaC`, nous considérons les variables :
- `NIR_ANO_17` : identifiant du bénéficiaire ([fiche identifiant des bénéficiaires](../fiches/fiche_beneficiaire.md))
- `EXE_SOI_DTD` et `EXE_SOI_DTF` qui indiquent les dates d'entrée et de sortie à l'hôpital respectivement  
  
Pour 2016, nous ne disposons que des données de facturation "brutes" transmises par l'établissement de la table `T_SSRaaSTC`.  

Dans la table `T_SSRaaSTC`, qui est la **table de prise en charge des séjours dans le public** (données brutes renseignées par l'établissement), on s'intéresse aux variables suivantes :  
- `REM_BAS_MNT` : coût du séjour calculé par l'établissement (sert de base de remboursement pour le TM) 
- `TOT_MNT_AM` : coût du séjour facturé à l'AM selon les règles de calcul de l'établissement (différent du montant remboursé par l'AM qui est calculé sur la base du GHS) 
- `FAC_MNT_TM` : montant à facturer au titre du ticket modérateur (TM)
- `FAC_MNT_FJ` : montant à facturer au titre du forfait journalier (FJ)
- `NAT_ASS` : nature d’assurance (mêmes modalités que décrites pour le MCO) 
- `EXO_TM` : code justification d’exonération du ticket modérateur (mêmes modalités que décrites pour le MCO) 
- `FJ_COD_PEC` : code de prise en charge du forfait journalier (mêmes modalités que décrites pour le MCO) 
- `FAC_18E` : facturation (1) ou non (0) de la participation forfaitaire de 18 € (2016)
- `REM_TAU`: taux de remboursement du séjour

Dans la table `T_SSRaaS`, qui est la **table de synthèse du séjour**, nous considérons les variables :
- `ENT_MOD` : mode d'entrée dans le champ PMSI SSR
- `SOR_MOD` : mode de sortie du champ PMSI SSR (mêmes modalités que décrites pour le MCO) 
- `PRE_JOU_NBR` : nombre de jours de présence
- `SEJ_NBJ` : durée (totale) du sejour

Dans la table `T_SSRaaB`, qui est la **table de description du séjour**, nous considérons les variables :
- `HOS_TYP_UM` : type d'hospitalisation UM, qui permet de séparer les hospitalisations complètes et partielles, sachant qu'il y a un unique mode d'hospitatalisation par séjour. 
Cette variable prend les valeurs suivantes :
  * code 1 : hospitalisation complete 
  * code 2 : hospitalisation partielle de jour 
  * code 3 : hospitalisation partielle de nuit
  * code 4 : hospitalisation partielle de séances  
- `GRG_GME` : groupe médico-économique attribué au séjour

### Filtres à ajouter

Les filtres à poser pour extraire les informations sur les dépenses et le reste à 
charge lors de séjours en SSR en établissements publics sont détaillées dans la 
[fiche sur les dépenses à l'hôpital public](../fiches/depenses_hopital_public.md).

### Méthodologie d'exploitation du PMSI SSR pour le calcul du reste à charge

#### Nettoyage des taux de remboursement

Le taux de remboursement est indiqué par la variable `REM_TAU` (table `T_SSRaaSTC`) fournie par l'établissement (sans avoir été corrigée par l'ATIH).  

Il est possible de reconstruire ce taux comme indiqué dans la [documentation de l'ATIH](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwj2rJaj-OTlAhU2D2MBHWvqAHMQFjAAegQIABAH&url=https%3A%2F%2Fwww.atih.sante.fr%2Fplateformes-de-transmission-et-logiciels%2Flogiciels-espace-de-telechargement%2Ftelecharger%2Fgratuit%2F9797%2F2100&usg=AOvVaw3JFAfMc9byZDL3-VO5C0Yj). La construction de la variable `tx_ATIH` est détaillée ci-dessus dans la méthodologie d'exploitation du PMSI MCO. 

*Suggestion :*  
Nous suggérons la création de la variable corrigée `TAUX_C` comme suit :
1. `TAUX_C` est égal à `tx_ATIH` (sauf si manquant) 
2. Si `tx_ATIH` manquant et `REM_TAU` non nul ni manquant : `TAUX_C` est égal à `REM_TAU`
3. Si `REM_TAU` nul ou manquant, supprimer la ligne

On peut également attribuer un taux de remboursement corrigé (`TAUX_C`) de 100 % dans les cas où la participation forfaitaire s'applique.  

#### Nettoyage du forfait journalier

Le montant du forfait journalier est renseigné par la variable `FAC_MNT_FJ` (table `T_SSRaaSTC`).  

*Suggestion :* 
Il est également possible de recalculer le montant du forfait journalier à partir du nombre de jours de présence (variable `PRE_JOU_NBR`), 
du type d'hospitalisation (`HOS_TYP_UM`), du code d'exonération du forfait journalier (`FJ_COD_PEC`), 
des frais d'hébergement par journée d’hospitalisation (18€ en 2016) et du mode de sortie (`SOR_MOD`), comme suit : 
- `FJ_C`= 0 en cas de séjour en ambulatoire (entrée / sortie le même jour)
- `FJ_C`= 0 en cas de séjour en hospitalisation partielle
- `FJ_C`= 0 en cas d'exonération du forfait journalier (*i.e.* si `FJ_COD_PEC`="R" et si les conditions d'exonération listées dans la partie intitulée "Le forfait journalier et le forfait journalier de sortie" sont remplies)
- `FJ_C`= 18 * `PRE_JOU_NBR`-1, si transfert ou décès (on ne compte pas le jour de sortie)
- `FJ_C` = 18 * (`PRE_JOU_NBR`), sinon

On peut ensuite créer une variable corrigée appelée `FJ_C2` qui prend la valeur du forfait journalier effectivement facturé : 
- `FJ_C2` = 0 € si séjour en ambulatoire, en hospitalisation partielle ou si exonération du FJ
- `FJ_C2` = 18 € (= valeur du FJ du jour de sortie en 2016) si séjour sans exonération de TM ni de FJ, avec moins de 30 jours de présence, sans transfert ni décès
- `FJ_C2` = 0 € si séjour sans exonération de TM ni de FJ, avec moins de 30 jours de présence, avec transfert ou décès
- `FJ_C2` = `FJ_C` - 30*18 (= valeur du FJ au-delà du 30ème jour de présence) si séjour sans exonération de TM ni de FJ, avec plus de 30 jours de présence, sans transfert ni décès 
- `FJ_C2` = `FJ_C` - 29*18 (= valeur du FJ au-delà du 30ème jour de présence + FJ du jour de sortie) si séjour sans exonération de TM ni de FJ, avec plus de 30 jours de présence, avec transfert ou décès 
- `FJ_C2` = `FJ_C` sinon (=valeur du FJ pour l'ensemble du séjour)

*ATTENTION > en cas de séjours contigus, la limite des 30 jours s'applique à la durée d'hospitalisation cumulée. Pour plus de rigueur, il faudrait chaîner les séjours pour calculer la durée de séjour, non pas pas PMSI, mais depuis l'entrée à l'hôpital.*  


#### Nettoyage du ticket modérateur

Tout comme en MCO, nous suggérons de recalculer le montant du ticket modérateur pour tenir compte 
de son plafonnement à 30 jours en cas de séjours longs d'un assuré de droit commun.  
Contrairement au MCO, on n'utilise pas la durée du séjour mais le nombre de jours de présence (`PRE_JOU_NBR`).  

*Suggestion :*  
Nous suggérons la création de la variable corrigée `TM_C` comme suit :  
1. Pour le séjour d'un assuré de droit commun de plus de 30 jours :
    - Nous pensons que `FAC_MNT_TM` = 20 % * TJP * `PRE_JOU_NBR`
    - Nous recalculons un ticket modérateur corrigé plafonné à 30 jours :   
      `TM_C` = 20 % * TJP * 30 = `FAC_MNT_TM` * 30 / `PRE_JOU_NBR`
2. Dans tous les autres cas : `TM_C` = `FAC_MNT_TM`

#### Autres valeurs manquantes 
Pour faciliter le calcul du RAC, penser à remplacer d'éventuelles valeurs manquantes de `FAC_18E` et `FAC_MNT_TM` par des 0.

#### Calcul du RAC

On utilise les variables suivantes :  
- `TAUX_C`: taux de remboursement du séjour (corrigé)
- `FJ_C`: montant du forfait journalier pour l'ensemble du séjour (corrigé)
- `FJ_C2`: montant du forfait journalier facturé (en fonction de la durée de séjour, de l'imputation du FJ sur le TM)
- `FAC_18E`: montant à facturer au titre de la participation forfaitaire de 18 € (table `T_SSRaaSTC`)
- `TM_C` : montant à facturer au titre du ticket modérateur (version corrigé de `FAC_MNT_TM` de la table `T_SSRaaSTC`)
- `FJ_COD_PEC`: code de prise en charge du forfait journalier (table `T_SSRaaSTC`)


En SSR, on distingue 5 cas de figure pour le calcul du RAC AMO.  
::: warning CALCUL DU RAC AMO
Calcul de la variable `rac` dans les différents cas de figure :  
1. `rac` = 0 si `FJ_COD_PEC`==R & `TAUX_C`==100 (exonération de TM et de FJ)
2. `rac` = `FAC_18`*18 + `FJ_C2` si `FJ_COD_PEC`!=R & `TAUX_C`==100 (exonération de TM mais pas de FJ)
3. `rac` = `TM_C` si `FJ_COD_PEC`==R & `TAUX_C`!=100 (exonération de FJ mais pas de TM)
4. `rac` = `TM_C`+`FJ_C2` si `FJ_COD_PEC`!=R & `TAUX_C`!=100 & `TM_C` > `FJ_C` (aucune exonération, TM>FJ)
5. `rac` = `FJ_C` si `FJ_COD_PEC`!=R & `TAUX_C`!=100 & `TM_C` < `FJ_C` (aucune exonération, FJ>TM)
:::

Le coût total du séjour correspond au montant remboursé par l’AMO (*cf.* [fiche sur les dépenses de santé](../fiches/depenses_hopital_public.md)) auquel on ajoute le reste à charge. 

*Pour aller plus loin >*  
- Il est ensuite possible de calculer un reste à charge après AMO **par bénéficiaire** en agrégeant les RAC pour les différents séjours d'un même bénéficiaire (en utilisant le `NIR_ANO_17` de la table `T_SSRaaC`).
- Il est possible d'aller chercher les parts supplémentaires prises en charge par le public pour les bénéficiaires de la CMU-C
  comme décrit dans la partie sur le MCO. 
  
## En pratique : calcul des restes à charge hospitaliers à partir du PMSI HAD

Nous décrivons ci-dessous les modalités de calcul du reste à charge opposable à partir des données du PMSI HAD de 2016.  

### Les tables et variables mobilisées

Pour plus d'informations sur les tables et variables utilisées, se référer à la [fiche sur les dépenses](../fiches/depenses_hopital_public.md).

La clef de chaînage entre les tables mentionnées ci-dessous est le couple (`ETA_NUM_EPMSI`, `RHAD_NUM`) où `ETA_NUM_EPMSI` est le numéro FINESS de l'établissement 
et `RHAD_NUM` le numéro séquentiel du séjour. 

Dans la **table de chaînage patients** `T_HADaaC`, nous considérons les variables :
- `NIR_ANO_17` : identifiant du bénéficiaire ([fiche identifiant des bénéficiaires](../fiches/fiche_beneficiaire.md))
- `EXE_SOI_DTD` et `EXE_SOI_DTF` qui indiquent respectivement les dates de début et de fin de la prise en charge à domicile 

La table `T_HADaaVALO` de valorisation des séjours (données retraitées par l'ATIH) est disponible sur le portail de l'ATIH uniquement. Il faut privilégier son utilisation si elle est disponible.   
Sur le portail CNAM, nous ne disposons que des données de facturation "brutes" transmises par l'établissement de la table `T_HADaaSTC`.  

Dans la table `T_HADaaSTC`, qui est la **table de prise en charge des séjours dans le public** (données brutes renseignées par l'établissement), on s'intéresse aux variables suivantes :  
- `REM_BAS_MNT` : coût du séjour calculé par l'établissement (sert de base de remboursement pour le TM) 
- `TOT_MNT_AM` : coût du séjour facturé à l'AM selon les règles de calcul de l'établissement (différent du montant remboursé par l'AM qui sera calculé sur la base du GHS) 
- `FAC_MNT_TM` : montant à facturer au titre du ticket modérateur (TM)
- `NAT_ASS` : nature d’assurance (mêmes modalités que décrites pour le MCO) 
- `EXO_TM` : code justification d’exonération du ticket modérateur (mêmes modalités que décrites pour le MCO) 
- `FAC_18E` : facturation (1) ou non (0) de la participation forfaitaire de 18 € (2016)
- `REM_TAU`: taux de remboursement du séjour

Dans la table `T_HADaaS`, qui est la table de synthèse des résumés anonymes par sous-séquence, nous considérons les variables :
- `HAD_DUREE` : durée couverte par les séquences
- `SEJ_NBJ` : nombre de journées dans le séjour

Dans la table `T_HADaaGRP`, qui est la table de groupage, nous considérons les variables :
- `GHT_NUM` : groupe homogène de tarif ([GHT](../glossaire/GHT.md))
- `PAP_GRP_GHPC` : groupe homogène de prise en charge

### Filtres à ajouter

Les filtres à poser pour extraire les informations sur les dépenses et le reste à 
charge lors de séjours en HAD en établissements publics sont détaillées dans la 
[fiche sur les dépenses à l'hôpital public](../fiches/depenses_hopital_public.md).

### Méthodologie d'exploitation du PMSI HAD pour le calcul du reste à charge

#### Nettoyage des taux de remboursement

Le taux de remboursement est indiqué par la variable `REM_TAU` (table `T_HADaaSTC`) fournie par l'établissement (sans avoir été corrigée par l'ATIH).  

Il est possible de reconstruire ce taux comme indiqué dans la [documentation de l'ATIH](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwiRm-798-TlAhUPFRQKHQHkBi8QFjACegQIABAH&url=https%3A%2F%2Fwww.atih.sante.fr%2Fplateformes-de-transmission-et-logiciels%2Flogiciels-espace-de-telechargement%2Ftelecharger%2Fgratuit%2F8758%2F1745&usg=AOvVaw3nx5ugXpZiyo3SBrv_M4is). La construction de la variable `tx_ATIH` est détaillée ci-dessus dans la méthodologie d'exploitation du PMSI MCO.

*Suggestion :*  
Nous suggérons la création de la variable corrigée `TAUX_C` tout comme en SSR. 

#### Nettoyage du ticket modérateur

Tout comme en MCO, nous suggérons de recalculer le montant du ticket modérateur (variable corrigée `TM_C`) pour tenir compte 
de son plafonnement à 30 jours en cas de séjours longs d'un assuré de droit commun.  


#### Autres valeurs manquantes 

Pour faciliter le calcul du RAC, penser à remplacer d'éventuelles valeurs manquantes de `FAC_18E` et `FAC_MNT_TM` par des 0.

#### Calcul du RAC

Par définition, il n'y a pas de forfait journalier en hospitalisation *à domicile*, ce qui simplifie le calcul du reste à charge.  

On utilise les variables suivantes :  
- `TAUX_C`: taux de remboursement du séjour (corrigé)
- `FAC_18E`: montant à facturer au titre de la participation forfaitaire de 18 € (table `T_HADaaSTC`)
- `TM_C` : montant à facturer au titre du ticket modérateur (version corrigé de `FAC_MNT_TM` de la table `T_HADaaSTC`)

En HAD, on distingue deux cas de figure pour le calcul du RAC AMO.  
::: warning CALCUL DU RAC AMO
Calcul de la variable `rac` dans les différents cas de figure :  
1. `rac` = `FAC_18`*18 si `TAUX_C`==100 (pas de TM, éventuellement PF si `FAC_18` == 1)
2. `rac` = `TM_C` si `TAUX_C`!=100 (pas d'exonération de TM)
:::

Le coût total du séjour correspond au montant remboursé par l’AMO (*cf.* fiche sur les dépenses à l'hôpital public) auquel on ajoute le reste à charge du séjour.

*Pour aller plus loin >*  
- Il est ensuite possible de calculer un reste à charge après AMO **par bénéficiaire** en agrégeant les RAC pour les différents séjours d'un même bénéficiaire (en utilisant le `NIR_ANO_17` de la table `T_HADaaC`).
- Il est possible d'aller chercher les parts supplémentaires prises en charge par le public pour les bénéficiaires de la CMU-C comme décrit dans la partie sur le MCO.

## Références

*Sources :*  
- Publication de référence utilisant la méthode proposée dans la présente fiche [Hospitalisation : des restes à charge après assurance maladie obligatoire plus élevés en soins de suite et de réadaptation et en psychiatrie](https://drees.solidarites-sante.gouv.fr/sites/default/files/2021-05/ER1192.pdf)
- Code de la sécurité sociale : articles L174-4, L.322-2 et L.322-3, L325-1, D 325-1, D 325-6, L 861-3, R 174-5, R 174-5-1 et R 174-5-2
- [Fiche du Ministère de la santé sur la facturation du forfait journalier hospitalier](https://solidarites-sante.gouv.fr/professionnels/gerer-un-etablissement-de-sante-medico-social/financement/regles-de-facturation/article/la-facturation-les-regles)
- [Fiche du Ministère de la santé sur les tarifs de prestation](https://solidarites-sante.gouv.fr/IMG/pdf/tarifs_de_prestations-3.pdf)
- [Fiche du Ministère de la santé sur la facturation du ticket modérateur forfaitaire](https://solidarites-sante.gouv.fr/IMG/pdf/ticket_moderateur_forfaitaire_de_18eur.pdf)
- [Projet de loi de financement de la sécurité sociale pour 2020](http://www.assemblee-nationale.fr/15/projets/pl2296.asp)
- [ATIH - Manuel d'utilisation du logiciel VisualValoSej (PMSI MCO)](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=3&ved=2ahUKEwiRm-798-TlAhUPFRQKHQHkBi8QFjACegQIABAH&url=https%3A%2F%2Fwww.atih.sante.fr%2Fplateformes-de-transmission-et-logiciels%2Flogiciels-espace-de-telechargement%2Ftelecharger%2Fgratuit%2F8758%2F1745&usg=AOvVaw3nx5ugXpZiyo3SBrv_M4is) 
- [ATIH - Manuel d'utilisation du logiciel VisualValo SSR](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=1&ved=2ahUKEwj2rJaj-OTlAhU2D2MBHWvqAHMQFjAAegQIABAH&url=https%3A%2F%2Fwww.atih.sante.fr%2Fplateformes-de-transmission-et-logiciels%2Flogiciels-espace-de-telechargement%2Ftelecharger%2Fgratuit%2F9797%2F2100&usg=AOvVaw3JFAfMc9byZDL3-VO5C0Yj)
- Supports de formation de la CNAM sur le DCIRS et le PMSI


::: tip Crédits

Cette fiche a été rédigée par Noémie Courtejoie (DREES), Raphaële Adjerad (DREES) et Kristel Jacquier (DSS)
:::


